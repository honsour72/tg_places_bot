from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from asyncio import run
from tg_places_bot.config import API_TOKEN
from tg_places_bot.handlers.user import register_user_handlers


async def main():
    bot = Bot(token=API_TOKEN)
    dp = Dispatcher(bot, storage=MemoryStorage())

    # Регистрация хэндлеров
    register_user_handlers(dp)

    # Запуск поллинга
    try:
        await dp.start_polling()
    finally:
        s = await bot.get_session()
        await s.close()


def run_bot():
    try:
        run(main())
    except KeyboardInterrupt:
        print("You stopped the program")


if __name__ == '__main__':
    run_bot()
