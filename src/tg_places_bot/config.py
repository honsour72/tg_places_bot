import re
import configparser


CONFIG_NAME = 'bot.ini'
config = configparser.ConfigParser()
config.read(CONFIG_NAME)

DEBUG = config['bot']['debug']
API_TOKEN = config['bot']['api_token']
PAY_TOKEN = config['bot']['pay_token']
DATABASE_CREDS = dict(config['database_creeds'])
PLACE_CODE_LENGTH = config['constants']['place_code_length']

LOG_PATH = "src/tg_places_bot/logs/{}.log"
# LOG_USER_PATH = "/var/log/tg_places_bot/user/{}.log"
# LOG_PATH = "/var/log/tg_places_bot/"
LOG_FORMAT = "{time:YYYY:MMM:DD:ddd:HH:mm:ss} | {level} | {name}:{function}:{line} | {message}"

# =========================================== TEXT CONFIGURATIONS ==================================================== #

START = "start"
PARTNERSHIP = "partnership"
CITY = 'city'
CATEGORY = 'category'
TOP_PLACES = 'top_places'
REPORT = 'report_a_violation'
OFFER = 'leave_an_offer'
SUPPORT = 'once_support'
CITY_LOCATION = "city_location"
PLACE_LOCATION = 'place_location'
RATING = 'rating'
STAR = 'star'
MAIN_MENU = 'main_menu'
ADD_PLACE = 'add_place'
SHOW_READY_PLACE = 'show_ready_place'
EDIT_NEW_PLACE = "edit_new_place"
EDIT_NEW_PLACE_NAME = "edit_new_place_name"
EDIT_NEW_PLACE_DESCRIPTION = "edit_new_place_description"
EDIT_NEW_PLACE_PHOTO = "edit_new_place_photo"
EDIT_NEW_PLACE_LOCATION = "edit_new_place_location"
UPLOAD_NEW_PLACE_TO_DB = 'upload_place_to_db'

TEXTS = {
    START: "Привет!\nЯ - My City Bot!\n\nЯ проведу экскурсию по известному городу. На текущий момент мне известно "
           "уже 4 города и суммарно 6 разных мест в них!\n\nЯ умею в:\n\n"
           "✅ Разные города\n"
           "✅ Топ мест по городу\n"
           "✅ Различные категории в городах\n"
           "✅ Различные места в категориях\n"
           "✅ Рейтинг месту\n\n"
           "Давай начнём!",
    CITY: "Мне пока известны только эти города, выберите один из них!",
    PARTNERSHIP: "У меня есть свой канал в телеграмме:\n@it_helper_s\n"
                 "И даже на youtube:\nhttps://www.youtube.com/@ITHelpers170\n"
                 "Подпишись, плиз🙏\n\n"
                 "Руководство проекта состоит всего из одного человека. А аренда хорошего сервера для работы этого "
                 "бота требует регулярных вложений.\n\nЯ буду очень рад любой финансовой поддержке!",
    CATEGORY: "Категории в этом городе:",
    TOP_PLACES: "Места с наибольшим рейтингом:\n(наибольшим количеством ⭐️)",
    'add_place_client': "Для того чтобы добавит место с локацией и фото необходимо стать редактором!",
    "add_place_editor": 'Отправьте мне фотографию места в следующем формате:\n\n'
                        'Название места\nОписание\nСмайлик[опционально]\n\n'
                        'Следующим сообщением нужно будет отправить геолокацию места',
    "editor_add_location": "Отлично, а теперь пришлите мне локацию этого места!\n\nТолько будьте внимательны: "
                           "если Вы используете приложение телеграмма на стационарном компьютере (не веб-версию) "
                           "тогда используйте следующий формат сообщения:\n\nширота\nдолгота\n\nНапример:\n"
                           "56.5803600\n39.0870136",
    REPORT: "Пришлите сообщение с жалобой, а я перешлю его администратору❗️\n\nВместе мы остановим этот беспредел ⛔️",
    OFFER: "Я Вас слушаю!\n\nПришлите мне сообщение, а я передам его администратору",
    SUPPORT: "Разовый донат:\nhttps://donatepay.ru/donation\n\n"
             "Мои креды:\n\nномер карты:\n123\nкиви-кошелек:\n456\nкрипто-кошелек:\n789",
    ADD_PLACE: "Пришлите название нового места",
    ADD_PLACE + "_name": "Пришлите описание нового места",
    ADD_PLACE + "_description": "Пришлите 1 фотографию нового места (именно одну именно фотографию а не документ)\n\n"
                                "Про то как это сделать:",
    ADD_PLACE + "_photo": "Пришлите локацию (либо координаты широты/долготы двумя строками)",
    ADD_PLACE + "_location": "Поздравляю, мы почти закончили!\n\nНовое место выглядит вот так:",
    EDIT_NEW_PLACE: "Выберите параметр для редактирования:",
    EDIT_NEW_PLACE_NAME: "Пришлите новое название",
    EDIT_NEW_PLACE_DESCRIPTION: "Пришлите новое описание",
    EDIT_NEW_PLACE_PHOTO: "Пришлите новое фото",
    EDIT_NEW_PLACE_LOCATION: "Пришлите новую локацию"
}

ADMIN_TEXTS = {
    "start": "Здравствуй, создатель",
    "city": "Все города",
    "category": "Все категории",
    "place": "Все места",
    "tg_user": "Все пользователи бота",
    "editor": "Все редакторы",
    "add_city": "Формат сообщения для добавления города:\n\ncode\nname\nlatitude\nlongitude\ndescription\nsmile",
    "add_category": 'Формат сообщения для добавления категории:\n\ncode\nname\nsmile',
    "add_place": 'Формат сообщения для добавления места:\n\nОтправить изображение (если с компа - то сжатое) '
                 'с подписью в следующем формате:\n\n'
                 'code\nname\nlatitude\nlongitude\ncategory_id\ndescription\nsmile\ncity_id',
    "add_tg_user": "Формат сообщения для добавления юзера:\n\ntelegram_id(unique)\nusername[optional]\nstatus",
    "add_editor": "Формат сообщения для добавления редактора:\n\nid(required)",
    "default": "Дефолтный текст для одмена из словаря admin_texts",
}


DEFAULT_TEXT = "Это дефолтный текст, он применяется в функции answer, при получении ключа text из response"
YOUR_MESSAGE_SENT = "Ваше сообщение передано администратору! 🤝"
DEFAULT_QUERY_ANSWER = ""

ITEM_REGEXP = re.compile("^(city|place|star|category|back_place)_\d+$")
LOCATION_FROM_USER_REGEXP = re.compile("^\d+.\d+\n\d+.\d+$")

ADD_PLACE_REGEXP = re.compile("^.+\n.*\n.*$")
ADMIN_ITEM_REGEXP = re.compile("^(user|tg_user|editor|city|category|place)_\d+$")

ADMIN_STATE_ITEM = re.compile("^Admin:(tg_user|city|category|place|editor)$")
ADMIN_ADD_ITEM_REGEXP = re.compile("^add_([a-zA-Z_]+)$")

ADMIN_EDIT_ITEM = re.compile("^edit_(city|category|tg_user|editor|place)_\d+$")
ADMIN_EDIT_ITEM_PARAMETER = re.compile("^(city|category|tg_user|editor|place)_(\d+)_(.+)$")

ADMIN_EDIT_CATEGORY = re.compile("^category_[code|name|smile]+$")

# =========================================== BUTTON REPRESENTATION TEXT ============================================= #

TO_MAIN_MENU = "◀️ В главное меню"
TO_PREVIOUS_STEP = "🔙 Назад"
TO_CITY = "️🏙 В город"
TO_CITIES = "️🏙 В города"
TO_CATEGORIES = "🔙 В категории"
TO_CATEGORY = "🔙 В категорию"
TO_PLACE = "🔙 Назад"
ADMIN_BACK = "Назад"
CLIENT_BACK = "️↩️ Назад"
ADD_PLACE_TEXT = '➕ Добавить место'
NEW_PLACE_DONE = "✅ Готово"
BACK_TO_MAIN_MENU = {"callback_data": MAIN_MENU, "text": TO_MAIN_MENU}
BACK_TO_PARTNERSHIP = {"callback_data": PARTNERSHIP, "text": CLIENT_BACK}

ALL_BACK_BUTTONS = [
    'back_to_city',
    'back_to_cities',
    'back_to_category',
    'back_to_categories',
    'back_to_place'
]

START_BUTTONS = [
    {"callback_data": CITY, "text": "🏢 Выбери город"},
    {"callback_data": PARTNERSHIP, "text": "🤝 По всем вопросам"}
]

PARTNERSHIP_BUTTONS = [
    # {"callback_data": "became_editor", 'text': "✏️ Стать редактором"},
    {"callback_data": SUPPORT, "text": "💳 Поддержать разово"},
    {"callback_data": OFFER, "text": "💡 У меня предложение!"},
    {"callback_data": REPORT, "text": "❗️ Сообщить о нарушении"},
    {"callback_data": MAIN_MENU, "text": TO_MAIN_MENU}
]

CITY_BUTTONS = [
    {"callback_data": CATEGORY, "text": "🗺 Куда сходить?"},
    {"callback_data": TOP_PLACES, "text": "🔝 Покажи топ мест"},
    {"callback_data": CITY_LOCATION, "text": "📍 Дай локацию"},
    {"callback_data": CITY, "text": "️🏙 В города"}
]

REPORT_BUTTONS = OFFER_BUTTONS = SUPPORT_BUTTONS = [BACK_TO_PARTNERSHIP]

PLACE_BUTTONS = [
    {"callback_data": PLACE_LOCATION, "text": "📍 Дай локацию"},
    {"callback_data": RATING, "text": "⭐️ Оценить"},
]

STARS_BUTTONS = [
    {"callback_data": f"{STAR}_{i}", "text": "⭐️" * i} for i in range(1, 6)
]

NEW_PLACE_BUTTONS = [
    {"callback_data": EDIT_NEW_PLACE, "text": "✏️ Редактируем"},
]

EDIT_NEW_PLACE_BUTTONS = [
    {"callback_data": EDIT_NEW_PLACE_NAME, "text": "⚡️ Название"},
    {"callback_data": EDIT_NEW_PLACE_DESCRIPTION, "text": "💬 Описание"},
    {"callback_data": EDIT_NEW_PLACE_PHOTO, "text": "🎆 Фото"},
    {"callback_data": EDIT_NEW_PLACE_LOCATION, "text": "📍 Локация"},
    {"callback_data": SHOW_READY_PLACE, "text": "👌 Всё ок"},
]

ADMIN_START_BUTTONS = {
    "city": "Города",
    "category": "Категории",
    "place": "Места",
    'tg_user': "Пользователи",
    'editor': "Редакторы"
}

ADMIN_EDIT_BUTTONS = {
    "edit_item": "✏️",
    "remove_item": "🗑",
}

COLUMN_BUTTONS = {
    # common shouldn't be used
    "id": "ID но этого не должно быть тут!",
    'username': "Username",

    # city + category
    "code": "Код",
    "name": "Название",
    "latitude": "Широта",
    "longitude": "Долгота",
    "description": "Описание",
    "smile": "Смайлик",

    # place
    "category_id": "ID категории",
    "photo": "Фото",
    "city_id": "ID города",
    "added_by": "Кто добавил",

    # tg_user + editor
    "status": "Статус",
}

KEYBOARD = {
    START: START_BUTTONS,
    PARTNERSHIP: PARTNERSHIP_BUTTONS,
    SUPPORT: SUPPORT_BUTTONS,
    OFFER: OFFER_BUTTONS,
    REPORT: REPORT_BUTTONS,
}

SIMPLE_QUERIES = [PARTNERSHIP, SUPPORT, OFFER, REPORT]
ITEMS = [CITY, CATEGORY]
LOCATIONS_QUERY = CITY_LOCATION, PLACE_LOCATION
EDITABLE_QUERIES = [EDIT_NEW_PLACE_NAME, EDIT_NEW_PLACE_DESCRIPTION, EDIT_NEW_PLACE_PHOTO, EDIT_NEW_PLACE_LOCATION]
