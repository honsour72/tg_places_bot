import psycopg2
from datetime import datetime
from psycopg2.errors import OperationalError
from transliterate import translit
from transliterate.exceptions import LanguageDetectionError


def connect_to_db(creeds):
    try:
        return DataBase(**creeds)
    except OperationalError:
        raise OperationalError(f"Cannot connect to database")


class DataBase:
    def __init__(self, name, host, port, user, password):
        self.log = None
        self.connection = psycopg2.connect(dbname=name, host=host, user=user, password=password, port=port)
        self.cursor = self.connection.cursor()

    def do_sql(self, sql_query: str, fetch_all: bool = True, with_error: bool = False):
        result = 0
        error = ''
        try:
            self.cursor.execute(sql_query)
            if fetch_all:
                result = self.cursor.fetchall()
                for i, res in enumerate(result):
                    res = list(res)
                    if 'FROM place' in sql_query:
                        res[7] = res[7].tobytes()
                    result[i] = res
            else:
                result = 1
        except Exception as err:
            self.log.error(f"Sql error occurred while processing query: {sql_query}. It says: {repr(err)}")
            result = -1
        finally:
            self.connection.commit()
            if with_error:
                return result, error
            return result

    def get_table_columns(self, table_name: str, without_id: bool = True, without_columns: list = None) -> list:
        """
        \n
        :param without_columns:
        :param without_id:
        :param table_name:
        :return:
        """
        sql_query = f"SELECT column_name FROM information_schema.columns WHERE table_name = '{table_name}';"
        self.log.debug(f"got query: {sql_query}")
        inner_columns = self.do_sql(sql_query)
        for ci in range(len(inner_columns)):
            inner_columns[ci] = inner_columns[ci][0]

        if without_id:
            try:
                del inner_columns[0]
            except IndexError as ind_err:
                self.log.error(
                    f"Can't delete index (first column) from columns list: {inner_columns} of table {table_name}"
                    f" because of: {ind_err}")

        if without_columns:
            for c in without_columns:
                inner_columns.remove(c)

        self.log.debug(f"Return columns: {inner_columns}")
        return inner_columns

    def select(self, table_name: str,
               for_keyboard: bool = False,
               params: dict = None,
               field_index: int = None,
               to_dict: bool = False
               ):
        """

        :param to_dict:
        :param field_index:
        :param table_name:
        :param for_keyboard:
        :param params: dictionary with key-value filter, f.e. {'category_id': 1, 'code': 'moscow'}
        :return:\n\n

        The base-case (when only table_name is specified) will return list with row-logs tuples, f.e:\n
        [(id1, code1, name1 . . .), (id2, code2, name2 . . .), . . .]\n
        This logs appear by execution this sql query:\n
        `SELECT * FROM <table_name>;` -> and fetching all results\n\n

        When params-dict was specified func will generate different sql query row:\n
        `SELECT * FROM <table_name> WHERE <param_name>=<param_field>;`\n
                                                       ^          ^\n
        if `param_field` is a string object func will add quotes here\n
        And return the same list\n\n

        if field_index flag got int -> func will return list of values provided this index in every row-tuple\n
        if success sql query empty list otherwise:\n
        ['city_1', 'city_2', 'city_3', . . .] or ['place_1', 'place_2', . . .] or [1, 2, 3, . . .] or []\n\n

        if for_keyboard was True -> func will return a dict with code like a key and name of item like a value:\n
        {
            'city_1': "New York",
            'city_2': "Madrid",
            . . .
        }\n
        is success sql query, empty dict otherwise\n\n
        Possible return types: [(), (), ...] or [] or { 'code1': 'Value 1', . . . } or {}
        """
        if params:
            parts = []
            for k, v in params.items():
                if type(v) == int:
                    parts.append(f"{k}={v}")
                else:
                    parts.append(f"{k}='{v}'")
            sql_query = f"SELECT * FROM {table_name} WHERE {' AND '.join(parts)} order by id;"
        else:
            sql_query = f"""SELECT * FROM {table_name} order by id;"""

        self.log.debug(f"Select method sql query: {sql_query} "
                       f"for_keyboard: {for_keyboard}, params: {params}, field_index: {field_index}")
        all_items = self.do_sql(sql_query)

        # if error while do sql query occurred
        if type(all_items) != list:
            self.log.error(f"Type of returned sql query is not a list! {all_items}")
            # if for_keyboard:
            #     return {}
            # else:
            return []
        else:
            if for_keyboard:
                keyboard = []
                table_name = 'user' if table_name == 'tg_user' else table_name
                for item in all_items:
                    # make unique identifier, f.e. city_1, category_12, place_404 for callback_data into inline keyboard
                    key_item = f"{table_name}_{item[0]}"
                    # keyboard[key_item] = item[2]
                    button = {'callback_data': key_item, "text": item[2]}
                    keyboard.append(button)
                return keyboard

            if type(field_index) == int:
                try:
                    return [p[field_index] for p in all_items]
                except IndexError as ind_err:
                    self.log.exception(
                        f"get field index={field_index} that is too big to all_items elements: {all_items}")
                    return []

            if to_dict:
                columns = self.get_table_columns(table_name, without_id=False)
                result = []
                for item in all_items:
                    item_dict = dict(zip(columns, item))
                    result.append(item_dict)

                if len(result) == 0:
                    return {}

                return result[0] if len(result) == 1 else result

            # if there is one element in all_items it may look like: [(id, code, name, . . .), ]
            # we need to release it from tuple to list braces
            return list(all_items[0]) if len(all_items) == 1 else all_items

    def update(self, table_name: str, _id: int, params: dict):
        """

        :param table_name:
        :param _id:
        :param params:
        :return:
        """
        if params:
            param_names = list(params.keys())
            # get existing row from db: [1, 'code', 'name', latitude, longitude, 'description', 'smile', . . . etc]
            item = self.select(table_name, params={'id': _id})
            # item = list(item[0]) if item else []
            if item:
                item_columns = self.get_table_columns(table_name, without_id=False)
                for parameter in param_names:
                    replace_index = item_columns.index(parameter)
                    item[replace_index] = params[parameter]

                if table_name == 'place':
                    if isinstance(item[7], memoryview):
                        item[7] = item[7].tobytes()
                    item[7] = psycopg2.Binary(item[7])

                columns = ", ".join(item_columns)
                values = ", ".join(map(lambda x: f"'{x}'" if type(x) == str else str(x), item))
                updates = ", ".join([f"{n} = excluded.{n}" for n in item_columns[1:]])
                sql_query = f"insert into {table_name} ({columns}) values ({values})" \
                            f" on conflict (id) do update set {updates};"
                operation_status = self.do_sql(sql_query, fetch_all=False)
            else:
                operation_status = 0
                self.log.exception(f"There is nothing to update id: {_id} returned empty list")
        else:
            operation_status = 0
            self.log.debug(f"you tried to update table: {table_name} but give no params to update: {params}")

        return operation_status

    def insert(self, table_name: str, args: tuple, full_result: bool = False) -> [int, dict]:
        args = self.validate_args(table_name, args)
        sql_operation_status = -1
        if args:
            if table_name == "city":
                sql_query = """insert into city(id, code, name, latitude, longitude, description, smile) values 
                                        ({}, '{}', '{}', {}, {}, '{}', '{}');""".format(*args)

            elif table_name == 'tg_user':
                sql_query = """insert into tg_user(id, username, status) values ({}, '{}', '{}');""".format(*args)

            elif table_name == 'category':
                sql_query = """insert into category(id, code, name, smile) values ({}, '{}', '{}', '{}');""".format(
                    *args)

            elif table_name == 'place':
                sql_query = """insert into place(
                            id, code, name, latitude, longitude, category_id, description, photo, smile, city_id, tg_user) 
                                    values ({}, '{}', '{}', {}, {}, {}, '{}', {}, '{}', {}, '{}');""".format(*args)

            elif table_name == 'rating':
                sql_query = """insert into rating(place_id, user_id, stars, date) values 
                                                    ({}, {}, {}, {});""".format(*args)

            else:
                raise "Я пока не знаю такой таблицы"

            sql_operation_status = self.do_sql(sql_query, fetch_all=False)

            if sql_operation_status == 1:
                if full_result:
                    columns = self.get_table_columns(table_name, without_id=False)
                    return dict(zip(columns, args))
                else:
                    return args[0]

        return sql_operation_status

    def validate_args(self, table_name: str, args: tuple) -> [tuple, int]:
        """
        Validate args list to insert it like a row into table via table_name\n\n
        Possible sequences of args for:\n

        City table:\n
        ('code', 'name', 'latitude', 'longitude', 'description', 'smile')\n
        ('code', 'name', 'latitude', 'longitude')\n
        \tAmount of args is from 4 up to 6\n

        Category table:\n
        ('code', 'name', 'smile')\n
        ('code', 'name')\n
        \tAmount of args is from 2 up to 3\n

        Tg user table:\n
        ('telegram_id', 'username', 'status')\n
        ('telegram_id', 'status')\n
        ('telegram_id')\n
        \tAmount of args is from 1 up to 3\n

        Rating table:\n
        ('place_id', 'user_id', 'star', 'date')\n
        ('place_id', 'user_id', 'star')\n
        \tAmount of args is from 3 up to 4\n

        Place table:\n
        *It can be only message with photo or location-message*
        ('name', 'latitude', 'longitude', 'category_id', 'description', 'photo', 'smile', 'city_id', 'tg_user')\n\n

        All datatypes:\n
        code, name, description, smile, tg_user - str
        :param table_name: string. Can be: city, category, place, tg_user or rating and excepting any other!\n
        :param args:
        :return:
        """
        # Generation and inserting id
        if table_name != 'tg_user':
            current_ids = self.select(table_name, field_index=0)  # all db ids
            if current_ids:
                possible_ids = [i for i in range(min(current_ids), max(current_ids)) if i not in current_ids]
                possible_id = max(current_ids) + 1 if not possible_ids else possible_ids.pop(0)
            else:  # when table is empty
                possible_id = 1
        else:
            possible_id = int(args[0])
        self.log.debug(f"One possible id for {table_name} is {possible_id}")

        if table_name == 'city':
            if 4 <= len(args) <= 6:
                code = args[0]
                name = args[1]
                latitude = args[2]
                longitude = args[3]
                description = args[4] if len(args) > 4 else ""
                smile = args[5] if len(args) > 5 else ""
                args = (possible_id, code, name, latitude, longitude, description, smile)
            else:
                columns = self.get_table_columns(table_name, without_id=False)
                self.log.error(f"Len of args != len of columns table {table_name}. Args: {args}, columns: {columns}")
                return 0

        elif table_name == 'category':
            if 2 <= len(args) <= 3:
                code = args[0]
                name = args[1]
                smile = args[2] if len(args) == 3 else ""
                args = (possible_id, code, name, smile)
            else:
                columns = self.get_table_columns(table_name, without_id=False)
                self.log.error(f"Len of args != len of columns table {table_name}. Args: {args}, columns: {columns}")
                return 0

        elif table_name == 'tg_user':
            telegram_id = args[0]
            if len(args) == 3:
                username = args[1]
                status = args[2]
            elif len(args) == 2:
                username = ''
                status = args[1]
            else:
                columns = self.get_table_columns(table_name, without_id=False)
                self.log.error(f"Len of args != len of columns table {table_name}. Args: {args}, columns: {columns}")
                return 0
            args = (telegram_id, username, status)

        elif table_name == 'rating':
            if 3 <= len(args) <= 4:
                place_id = args[0]
                user_id = args[1]
                star = args[2]
                if len(args) == 3:
                    raw_date = datetime.now()
                    date = psycopg2.Date(raw_date.year, raw_date.month, raw_date.day)
                else:
                    date = args[3]
                args = (place_id, user_id, star, date)
            else:
                columns = self.get_table_columns(table_name, without_id=False)
                self.log.error(f"Len of args != len of columns table {table_name}. Args: {args}, columns: {columns}")
                return 0

        elif table_name == 'place':
            if len(args) == 9:
                name = args[0]
                latitude = args[1]
                longitude = args[2]
                category_id = args[3]
                description = args[4]
                photo = psycopg2.Binary(args[5])
                smile = args[6]
                city_id = args[7]
                tg_user = args[8]
                try:
                    # code = translit(name, reversed=True).lower().replace(" ", "_")[:PLACE_CODE_LENGTH]
                    code = translit(name, reversed=True).lower().replace(" ", "_")
                    code: str = code.replace("'", '')
                    self.log.info(f'For place {name} i generated code: {code}')
                except LanguageDetectionError as lang_err:
                    self.log.exception(f"can't detect language of: {name}. It says: {lang_err}")
                    code = f'place_{possible_id}'
                args = (
                    possible_id, code, name, latitude, longitude, category_id, description, photo, smile, city_id,
                    tg_user
                )
            else:
                self.log.error(f"For place we send only 8 columns without id, and code. But args were: {args}")
                return 0

        else:
            self.log.error(f"I don't know this table: {table_name}")
            return 0

        self.log.debug(f"Finally validated args for table: {table_name} is: {args}")
        return args

    def drop_items(self, table_name: str, params: dict) -> str:
        """

        :param table_name:
        :param params:
        :return:
        """
        if params:
            field_name, field_value = params.popitem()
            if type(field_value) == int:
                sql_query = f"DELETE FROM {table_name} WHERE {field_name}={field_value}"
            # if type(field_value) == str:
            else:
                sql_query = f"DELETE FROM {table_name} WHERE {field_name}='{field_value}'"
        else:
            sql_query = f"DROP TABLE {table_name}"  # Don't work because must be owner of the table

        operation_status = self.do_sql(sql_query, fetch_all=False)
        return operation_status

    def get_top_places(self, city_id: int):
        sql_query = f"""select p.id as "Place ID", p.name as "Place name", round(sum(r.star) * 1.0 / count(r.star), 2) 
                        as "Rating" from rating r left join place p on p.id = r.place_id 
                        left join city on p.city_id = city.id where p.city_id={city_id} 
                        group by "Place ID", "Place name" order by "Rating" desc;"""
        all_top = self.do_sql(sql_query)
        if all_top:
            # keyboard = {}
            keyboard = []
            for t in all_top:
                # keyboard[f"place_{t[0]}"] = f"{t[1]} - {t[2]}⭐️"
                row = {"callback_data": f"place_{t[0]}",  "text": f"{t[1]} - {t[2]}⭐️"}
                keyboard.append(row)
            self.log.debug(f"return top places amount: {len(keyboard)} of city with id: {city_id}")
            return keyboard
        else:
            self.log.warning(f"There is no top places of city with id: {city_id}")
            return []

    def get_user_status(self, tg_user_id):
        sql_query = "select status from tg_user where id={}".format(tg_user_id)
        result = self.do_sql(sql_query, fetch_one=True)
        return result

    def get_admins(self):
        sql_query = "select id from tg_user where status='admin';"
        return self.do_sql(sql_query, fetch_one=True)

    def rate_place(self, place_id: int, user_id: int, stars: int):
        raw_date = datetime.now()
        date = psycopg2.Date(raw_date.year, raw_date.month, raw_date.day)
        sql_query = f"insert into rating (place_id, user_id, star, date) values " \
                    f"({place_id}, {user_id}, {stars}, {date}) on conflict (place_id) do update set " \
                    f"user_id = excluded.user_id, star = excluded.star, date = excluded.date;"
        # sql_query = f"insert into rating (place_id, user_id, star, date) values " \
        #             f"({place_id}, {user_id}, {stars}, {date});"
        result = self.do_sql(sql_query, fetch_all=False)
        return result
