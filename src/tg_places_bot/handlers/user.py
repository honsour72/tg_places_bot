import io
import inspect
from aiogram import Dispatcher, utils
from aiogram.dispatcher import FSMContext, filters
from aiogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery, ContentTypes
from asyncio import sleep
from tg_places_bot.config import *
from loguru import logger as log
from tg_places_bot.database.database import connect_to_db
from tg_places_bot.sdk.states import UserState
from tg_places_bot.sdk.utils import *


db = connect_to_db(DATABASE_CREDS)


async def start(event: Message | CallbackQuery, state: FSMContext):
    user_id = event.from_user.id
    log.add(LOG_PATH.format(user_id), colorize=True, format=LOG_FORMAT, enqueue=True, level='INFO')
    db.log = log

    text = TEXTS[inspect.stack()[0][3]]
    buttons = [InlineKeyboardButton(**button) for button in START_BUTTONS]
    keyboard = InlineKeyboardMarkup()
    keyboard.add(*buttons)

    await state.set_state(UserState.common.state)
    if isinstance(event, CallbackQuery):
        await event.answer()
        await event.message.edit_text(text=text, reply_markup=keyboard)
    else:
        log.info("User start's the bot")
        await event.answer(text=text, reply_markup=keyboard)


async def partnership(event: CallbackQuery | Message, state: FSMContext):
    if isinstance(event, CallbackQuery):
        query_data = event.data

        await event.answer()

        text = TEXTS[query_data]
        buttons = [InlineKeyboardButton(**button) for button in KEYBOARD[query_data]]
        keyboard = InlineKeyboardMarkup(row_width=1)
        keyboard.add(*buttons)

        if query_data in [REPORT, OFFER]:
            await state.set_state(UserState.user_offer_report.state)

        if query_data == PARTNERSHIP:
            await state.set_state(UserState.common.state)

        await event.message.edit_text(text=text, reply_markup=keyboard)

    else:
        await sleep(1)
        text = YOUR_MESSAGE_SENT
        keyboard = InlineKeyboardMarkup(row_width=1)
        keyboard.add(InlineKeyboardButton(**BACK_TO_PARTNERSHIP))
        await event.answer(text=text, reply_markup=keyboard)
        await state.set_state(UserState.common.state)
        await notice_admin(message=event, new_text=text, with_sound=True)


async def items(query: CallbackQuery, state: FSMContext):
    await query.answer()
    _item = query.data
    user_state = await state.get_state()

    text = TEXTS[_item]
    buttons = [InlineKeyboardButton(**b) for b in db.select(_item, for_keyboard=True)]

    if _item == 'city':
        buttons += [InlineKeyboardButton(**BACK_TO_MAIN_MENU)]
    else:  # category
        user_data = await state.get_data()
        city = user_data['city']
        callback_data = f"city_{city['id']}"
        buttons += [InlineKeyboardButton(callback_data=callback_data, text=TO_CITY)]

    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(*buttons)

    if user_state == f"{UserState.__name__}:delete_last_message":
        await state.set_state(UserState.common.state)
        await query.message.delete()
        await query.message.answer(text=text, reply_markup=keyboard)
    else:
        await query.message.edit_text(text=text, reply_markup=keyboard)


async def item(query: CallbackQuery, state: FSMContext):
    query_data = query.data

    edit_keyboard = False
    edit_text = True
    # edit_text = False

    if 'back_' in query_data:
        edit_keyboard = True
        query_data = query_data.replace('back_', "")

    code, index = query_data.split("_")
    user_state = await state.get_state()

    if code == 'star':
        await query.answer(f"Your rating {int(index) * '⭐️'} noticed, thank you!")
        user_data = await state.get_data()
        # if we rate place chosen from top_places there is no category in state at this moment --> use get()
        category_id = user_data.get('category', {'id': 1})['id']
        place_id = user_data['place']['id']
        user_id = query.from_user.id
        rated = db.rate_place(place_id=place_id, user_id=user_id, stars=int(index))
        if rated:
            log.info(f"Place with ID: {place_id} was rated {index} stars")
        else:
            log.error(f'There is no rate mark for place: {place_id} by user_id {user_id}. sql status: {rated}')

        buttons = [InlineKeyboardButton(**b) for b in PLACE_BUTTONS]
        callback_data = f"category_{category_id}"
        buttons += [InlineKeyboardButton(callback_data=callback_data, text=TO_CATEGORY)]
        row_width = 2
        keyboard = InlineKeyboardMarkup(row_width=row_width)
        keyboard.add(*buttons)
        code = 'place'
        edit_keyboard = True

    else:
        await query.answer()
        _item = db.select(code, params={'id': index}, to_dict=True)
        text = format_db_item_to_message_text(_item, log=log)

        if code == 'place':
            # await query.message.delete()
            user_data = await state.get_data()
            await state.update_data(place=_item)
            category_id = user_data.get('category', {'id': 1})['id']
            buttons = [InlineKeyboardButton(**b) for b in PLACE_BUTTONS]
            callback_data = f"category_{category_id}"
            buttons += [InlineKeyboardButton(callback_data=callback_data, text=TO_CATEGORY)]
            row_width = 2
            keyboard = InlineKeyboardMarkup(row_width=row_width)
            keyboard.add(*buttons)

        else:
            if user_state == f"{UserState.__name__}:delete_last_message":
                await state.set_state(UserState.common.state)
                await query.message.delete()
                edit_text = False  # ???

            if code == 'city':

                await state.update_data(city=_item)
                buttons = [InlineKeyboardButton(**b) for b in CITY_BUTTONS]
                row_width = 2

            else:  # category_N
                await state.update_data(category=_item)
                user_data = await state.get_data()
                city_id = user_data['city']['id']
                buttons = [
                    InlineKeyboardButton(**b)
                    for b in db.select('place', for_keyboard=True, params={'category_id': index, 'city_id': city_id})
                ]
                buttons += [
                    InlineKeyboardButton(callback_data=ADD_PLACE, text=ADD_PLACE_TEXT),
                    InlineKeyboardButton(callback_data=CATEGORY, text=TO_CATEGORIES)
                ]
                row_width = 1

    keyboard = InlineKeyboardMarkup(row_width=row_width)
    keyboard.add(*buttons)

    if code == "place":
        if edit_keyboard:
            await query.message.edit_reply_markup(reply_markup=keyboard)
            return
        await state.set_state(UserState.delete_last_message.state)
        await query.message.delete()
        await query.message.answer_photo(photo=io.BytesIO(_item['photo']),
                                         caption=text,
                                         reply_markup=keyboard)
        return
    # elif edit_text:
    #     await query.message.edit_text(text=text, reply_markup=keyboard)
    # else:
    #     await query.message.answer(text=text, reply_markup=keyboard)

    # EXPERIMENTAL  # fixme: if it needed!
    try:
        await query.message.edit_text(text=text, reply_markup=keyboard)
    except utils.exceptions.BadRequest:
        await query.message.answer(text=text, reply_markup=keyboard)


async def item_location(query: CallbackQuery, state: FSMContext):
    # get place or city
    item_name = query.data.rstrip('_location')  # city_location | place_location -> city | place
    _item = await state.get_data()
    item_data = _item[item_name]

    latitude = item_data['latitude']
    longitude = item_data['longitude']

    item_id = item_data['id']
    callback_data = f"{item_name}_{item_id}"
    buttons = [InlineKeyboardButton(callback_data=callback_data, text=CLIENT_BACK)]
    keyboard = InlineKeyboardMarkup()
    keyboard.add(*buttons)

    await query.answer()
    await query.message.delete()
    await state.set_state(UserState.delete_last_message.state)
    # await state.set_state(UserState.common.state)
    await query.message.answer_location(latitude=latitude, longitude=longitude, reply_markup=keyboard)


async def rate_place(query: CallbackQuery, state: FSMContext):
    user_data = await state.get_data()
    place_id = user_data['place']['id']

    keyboard = InlineKeyboardMarkup(row_width=2)
    buttons = [InlineKeyboardButton(**b) for b in STARS_BUTTONS]
    buttons = [buttons[0], buttons[3], buttons[1], buttons[4], buttons[2]]  # refresh stars order
    buttons += [InlineKeyboardButton(callback_data=f'back_place_{place_id}', text=CLIENT_BACK)]
    keyboard.add(*buttons)
    await query.message.edit_reply_markup(reply_markup=keyboard)


async def top_places(query: CallbackQuery, state: FSMContext):
    user_data = await state.get_data()
    city_id = user_data['city']['id']

    text = TEXTS[query.data]
    buttons = [InlineKeyboardButton(**b) for b in db.get_top_places(city_id)]
    buttons.append(InlineKeyboardButton(callback_data=f'city_{city_id}', text=TO_CITY))
    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(*buttons)

    await query.message.edit_text(text=text, reply_markup=keyboard)


async def add_place(query: CallbackQuery, state: FSMContext):
    query_data = query.data
    text = TEXTS[query_data]
    await query.answer()
    await state.set_state(UserState.place_name.state)
    await query.message.edit_text(text=text, reply_markup=None)


async def add_place_name(message: Message, state: FSMContext):
    # saving user sent text as place's name
    place_name = message.text
    new_place = {"name": place_name}
    await state.update_data(new_place=new_place)

    # generating a response
    suffix = add_place_name.__name__.rsplit('_', maxsplit=1)[-1]
    text = TEXTS[f"{ADD_PLACE}_{suffix}"]

    log.info(f"Place name: {repr(place_name)} added to {new_place}")
    await state.set_state(UserState.place_description.state)
    await message.answer(text=text)


async def add_place_description(message: Message, state: FSMContext):
    place_description = message.text
    user_data = await state.get_data()
    new_place = user_data['new_place']
    new_place['description'] = place_description
    await state.update_data(new_place=new_place)

    suffix = add_place_description.__name__.rsplit('_', maxsplit=1)[-1]
    text = TEXTS[f"{ADD_PLACE}_{suffix}"]

    log.info(f"Place description: {repr(place_description)} added to {new_place}")
    await state.set_state(UserState.place_photo.state)
    await message.answer(text=text)


async def add_place_photo(message: Message, state: FSMContext):
    if message.photo:
        photo_bytes = await get_photo_bytes(message_photo=message.photo)

        user_data = await state.get_data()
        new_place = user_data['new_place']
        new_place['photo'] = photo_bytes
        await state.update_data(new_place=new_place)

        suffix = add_place_photo.__name__.rsplit('_', maxsplit=1)[-1]
        text = TEXTS[f"{ADD_PLACE}_{suffix}"]

        log.info(f"Place photo bytes array length: {len(photo_bytes)}")
        await state.set_state(UserState.place_location.state)
        await message.answer(text=text)

    else:
        await remove_user_other_messages(message)
        return


async def add_place_location(message: Message, state: FSMContext):
    if message.location:
        latitude = message.location.latitude
        longitude = message.location.longitude
    else:
        latitude, longitude = message.text.split('\n')

    user_data = await state.get_data()
    new_place = user_data['new_place']
    new_place['latitude'] = latitude
    new_place['longitude'] = longitude
    log.info(f'Location lat:{latitude} and long:{longitude} added')

    await state.update_data(new_place=new_place)

    suffix = add_place_location.__name__.rsplit('_', maxsplit=1)[-1]
    text = TEXTS[f"{ADD_PLACE}_{suffix}"]

    await state.set_state(UserState.common.state)
    await message.answer(text=text)
    await sleep(2)
    await show_ready_place(message, state)


async def show_ready_place(event: Message | CallbackQuery, state: FSMContext):
    """
    Show result message of place representation and upload to database if it needed
    :param event:
    :param state:
    :return:
    """
    user_data = await state.get_data()
    new_place = user_data['new_place']
    category = user_data['category']
    category_id = category['id']

    text = format_db_item_to_message_text(new_place)
    photo_bytes = new_place['photo']
    keyboard = InlineKeyboardMarkup(row_width=2)
    buttons = [InlineKeyboardButton(**b) for b in NEW_PLACE_BUTTONS]
    buttons.append(InlineKeyboardButton(callback_data=UPLOAD_NEW_PLACE_TO_DB, text=NEW_PLACE_DONE))
    keyboard.add(*buttons)

    # if user added location right now this handler will be called in another message handler
    if isinstance(event, Message):
        # await event.delete()  # in first, this is no need to delete user location message
        await event.answer_photo(photo=photo_bytes, caption=text, reply_markup=keyboard)
    else:  # if user choose "Всё ок" in previous menu buttons this handler will be called like a CallbackQuery
        await event.message.delete()  # so, we need to remove last message in this case
        await event.answer()
        await event.message.answer_photo(photo=photo_bytes, caption=text, reply_markup=keyboard)


async def edit_new_place(query: CallbackQuery):
    """
    This handler works if user press on "Редактировать" button
    :param query:
    :return:
    """
    query_data = query.data
    text = TEXTS[query_data]
    keyboard = InlineKeyboardMarkup(row_width=2)
    buttons = [InlineKeyboardButton(**b) for b in EDIT_NEW_PLACE_BUTTONS]
    keyboard.add(*buttons)
    await query.answer()
    await query.message.delete()
    await query.message.answer(text=text, reply_markup=keyboard)


async def edit_new_place_field(query: CallbackQuery, state: FSMContext):
    """
    This handler works if user specify what place field he is going edit by pressing the same button e.g. edit_name
    :param query:
    :param state:
    :return:
    """
    query_data = query.data
    text = TEXTS[query_data]
    state_name = query_data.replace('_new_place', '')
    log.info(f"User set in {state_name}")
    await state.set_state(getattr(UserState, state_name).state)
    await query.answer()
    await query.message.answer(text=text)


async def save_edited_place_field(message: Message, state: FSMContext):
    """
    This handler catch user place's new field sent in message
    :param message:
    :param state:
    :return:
    """
    user_message = message.text

    user_state = await state.get_state()
    suffix = user_state.split('_')[-1]
    assert suffix in ('name', 'description', 'photo', 'location'), f"Strange suffix: {suffix}"

    user_data = await state.get_data()
    new_place = user_data['new_place']

    if suffix == 'photo':
        # photo_url = await message.photo[-1].get_url()
        # new_place[suffix] = urlopen(photo_url).read()
        new_place[suffix] = await get_photo_bytes(message_photo=message.photo)
    elif suffix == 'location':
        new_place['latitude'] = message.location.latitude
        new_place['longitude'] = message.location.longitude
    else:
        new_place[suffix] = user_message
    await state.update_data(new_place=new_place)  # update state data

    # await message.delete()
    await show_ready_place(message, state)
    await state.set_state(UserState.common.state)  # update user state


async def upload_new_place_to_db(query: CallbackQuery, state: FSMContext):
    """
    This handler will catch situation when user press "Готово" button
    :param query:
    :param state:
    :return:
    """
    user_data = await state.get_data()
    # response
    city = user_data['city']
    category = user_data['category']
    city_id = city['id']
    category_id = category['id']
    query.data = f"category_{category['id']}"
    # add place to database
    new_place = user_data['new_place']
    new_place['category_id'] = category_id
    new_place['city_id'] = city_id
    new_place['smile'] = ''
    new_place['tg_user'] = query.from_user.username
    args = new_place['name'], new_place['latitude'], new_place['longitude'], new_place['category_id'], \
        new_place['description'], new_place['photo'], new_place['smile'], new_place['city_id'], new_place['tg_user']
    insertion = db.insert('place', args)

    log_msg = f"User add new place. It's status: {insertion}"
    log.info(log_msg)
    await notice_admin(query.message, log_msg)
    await item(query, state)


async def notice_admin(message: Message, new_text: str = None, with_sound: bool = False, admin_id: int = 940310930):
    if not new_text:
        user_text = message.text
        user_name = message.from_user.username
        user_id = message.from_user.id
        new_text = f"User {f'@{user_name}' if user_name else f'with id:{user_id}'} send this:\n\n{user_text}"
    message.text = new_text
    await message.send_copy(chat_id=admin_id, disable_notification=with_sound)


async def remove_user_other_messages(message: Message, state: FSMContext):
    if message.text == '/start':
        await start(message, state)
        return
    username = message.from_user.username
    user_id = message.from_user.id
    log_message = f"User {f'@{username}' if username else f'with id: {user_id}'} send ditch: {message.text}"
    log.debug(log_message)
    await notice_admin(message, log_message)
    await message.delete()


def register_user_handlers(dp: Dispatcher):
    # START
    dp.register_message_handler(start, commands=['start'])
    dp.register_callback_query_handler(start, lambda q: q.data == MAIN_MENU, state=UserState.common)

    # PARTNERSHIP
    dp.register_callback_query_handler(
        partnership,
        lambda q: q.data in SIMPLE_QUERIES,
        state=[UserState.common, UserState.user_offer_report]
    )
    dp.register_message_handler(partnership, state=UserState.user_offer_report)

    # ITEMS: ['city', 'category']
    dp.register_callback_query_handler(items, lambda q: q.data in ITEMS,
                                       state=[UserState.common, UserState.delete_last_message])

    # ITEM:  city_1 | category_21 | place_404
    dp.register_callback_query_handler(
        item,
        lambda q: re.match(ITEM_REGEXP, q.data),
        state=[UserState.common, UserState.delete_last_message]
    )

    # ITEM_LOCATION: city_location | place_location
    dp.register_callback_query_handler(item_location, lambda q: q.data in LOCATIONS_QUERY,
                                       state=[UserState.common, UserState.delete_last_message])

    # RATE_PLACE: rating
    dp.register_callback_query_handler(rate_place, lambda q: q.data == RATING,
                                       state=[UserState.delete_last_message])

    # TOP PLACES: top_places
    dp.register_callback_query_handler(top_places, lambda q: q.data == TOP_PLACES, state=UserState.common)

    # ADD PLACE: add_place
    dp.register_callback_query_handler(add_place, lambda q: q.data == ADD_PLACE, state=UserState.common)

    # ADD PLACE NAME: add_place_name
    dp.register_message_handler(add_place_name, state=UserState.place_name)

    # ADD PLACE DESCRIPTION: add_place_description
    dp.register_message_handler(add_place_description, state=UserState.place_description)

    # ADD PLACE PHOTO: add_place_photo
    dp.register_message_handler(add_place_photo, state=UserState.place_photo, content_types=ContentTypes.ANY)

    # ADD PLACE LOCATION: add_place_location
    dp.register_message_handler(add_place_location,
                                filters.Regexp(regexp=LOCATION_FROM_USER_REGEXP),
                                state=UserState.place_location,
                                content_types=['location', 'text'])

    # SHOW READY PLACE: show_ready_place
    dp.register_callback_query_handler(show_ready_place, lambda q: q.data == SHOW_READY_PLACE, state=UserState.common)

    # EDIT NEW PLACE: edit_new_place
    dp.register_callback_query_handler(edit_new_place, lambda q: q.data == EDIT_NEW_PLACE, state=UserState.common)

    # EDIT NEW PLACE FIELD: edit_new_place_field
    dp.register_callback_query_handler(edit_new_place_field,
                                       lambda q: q.data in EDITABLE_QUERIES,
                                       state=UserState.common)

    # SAVE EDITED PLACE FIELD
    dp.register_message_handler(save_edited_place_field,
                                state=[
                                    UserState.edit_name, UserState.edit_description,
                                    UserState.edit_photo, UserState.edit_location
                                ],
                                content_types=['location', 'photo', 'document', 'text'])

    # UPLOAD NEW PLACE TO DB
    dp.register_callback_query_handler(upload_new_place_to_db, lambda q: q.data == UPLOAD_NEW_PLACE_TO_DB,
                                       state=UserState.common)

    # REMOVER: any other message
    dp.register_message_handler(remove_user_other_messages, state=UserState.common)
