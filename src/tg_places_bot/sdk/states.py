from aiogram.dispatcher.filters.state import StatesGroup, State


class UserState(StatesGroup):
    common = State()
    user_offer_report = State()
    delete_last_message = State()

    place_name = State()
    place_description = State()
    place_photo = State()
    place_location = State()

    edit_name = State()
    edit_description = State()
    edit_photo = State()
    edit_location = State()
