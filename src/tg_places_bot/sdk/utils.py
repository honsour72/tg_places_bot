import typing
from aiogram.types.photo_size import PhotoSize
from loguru import logger
from urllib.request import urlopen


def format_db_item_to_message_text(item_data: dict, log: logger = None) -> str:
    """
    Function for formatting city, category, place dict-presented logs to string for message text\n
    :param log:
    :param item_data:
    :return:
    """

    text = item_data['name'] + f" {item_data['smile'] if item_data.get('smile', None) else ''}"

    description: str = item_data.get('description', None)
    place_author: str = item_data.get('tg_user', None)

    if description:
        text += '\n\n' + item_data['description']

    if place_author and not place_author.isdigit():
        text += "\n\nДобавлено пользователем:\n@" + item_data['tg_user']

    if log:
        log.info(f"Generate text for item: {repr(text)}")
    return text


async def get_photo_bytes(message_photo: typing.List[PhotoSize], photo_index: int = -1):
    """
    We want to get high quality photo, so we use last index, e.g. -1
    :param message_photo: sequence with aiogram PhotoSize
    :param photo_index: integer - an index of photo in array. Last index - the most quality photo in array
    :return: bytes of picked photo
    """
    photo_url = await message_photo[photo_index].get_url()
    photo_bytes = urlopen(photo_url).read()
    return photo_bytes
