# Doc

## TODO

#### База данных
* Наполнить бд местами (опционально - городами)

#### Бот

* рефактор хендлера `item`
* Добавление мест пользователем - обработка ВСЕХ ситуаций
* админ может удалять места
* дописать тесты

## Быстрый старт

1. Run
    ```python
    pip install -e .
    ```
2. Run
    ```python
    run_bot
    ```

## Деплой

### В первый раз

1) Загрузить ключик на сервер
2) Сделать `git clone`
3) Создать `bot.ini` в корневой директории проекта
4) Создать юнит сервиса (`tg_bot.service`) `systemctl` примерного содержания:
    ```toml
    [Unit]
    Description=uWSGI instance to serve my cite
    After=network.target
    
    [Service]
    User=root
    Group=www-data
    WorkingDirectory=/var/www/my_cite
    Environment="PATH=/var/www/my_cite/flask_env/bin"
    ExecStart=/var/www/my_cite/flask_env/bin/uwsgi --ini main.ini
    
    [Install]
    WantedBy=multi-user.target
    ```
5) Дать команду: `systemctl start tg_bot.service`

### Обновление

1) Спулить актуальные изменения `git pull`
2) Перезапустить сервис `systemctl`: `systemctl restart tg_bot.service`

## Файловая архитектура проекта

```
tg_places_bot
├── database	
│   ├── __init__.py
│   └── database.py
│
├── handlers
│   ├── __init__.py
│   ├── admin.py
│   └── user.py
│
├── sdk
│   ├── __init__.py
│   ├── states.py
│   └── utils.py
│    
├── __init__.py
├── config.py
└── main.py
```

## База данных

Сущности:
* Место (place)
* Категория (category)
* Город (city)
* Пользователь (tg_user)
* Оценка места (rating)

## Хендлеры

1) start
2) partnership
3) items
4) item
5) item_location
6) rate_place
7) top_places
8) add_place
9) add_place_name
10) add_place_description
11) add_place_photo
12) add_place_location
13) remove_user_other_messages

## Примечания

Чтобы добавить команды под кнопку "меню" в левом нижнем углу, необходимо дать:

_Я это не использую в данном боте_

```python
from aiogram import Bot
from aiogram.types import BotCommand

bot = Bot(token='your_token')
start_cmd = BotCommand("start", "Начать")
order_cmd = BotCommand("help", "Помощь")
await bot.set_my_commands([start_cmd, order_cmd])
```

А чтобы убрать эту кнопку:

```python
await bot.delete_my_commands()
```

### Отдельная благодарность Тишке (Антон Тихонов - @Tishka17) за архитектуру
* [Статья про архитектуру](https://t.me/advice17/24)
* [aiogram doc](https://mastergroosha.github.io/aiogram-2-guide/quickstart/)

