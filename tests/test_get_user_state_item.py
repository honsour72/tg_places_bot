from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from src.tg_places_bot import get_user_state_item, db
import pytest


@pytest.mark.asyncio
async def test_get_user_state_all_items():
    test_state = FSMContext(chat=1, storage=MemoryStorage(), user=1)
    all_items = ('status', 'city', 'category', 'place', 'current_message_id')
    city_2 = db.select('city', params={'id': 2}, to_dict=True)
    category_2 = db.select('category', params={'id': 2}, to_dict=True)
    place_2 = db.select('place', params={'id': 2}, to_dict=True)

    await test_state.update_data(status='client')
    await test_state.update_data(city=city_2)
    await test_state.update_data(category=category_2)
    await test_state.update_data(place=place_2)
    await test_state.update_data(current_message_id=1000)

    for item_name in all_items:
        item = await get_user_state_item(test_state, item_name)
        if item_name == 'status':
            assert item == 'client'
        elif item_name == 'current_message_id':
            assert item == 1000
        else:
            assert isinstance(item, dict) is True


@pytest.mark.asyncio
async def test_get_user_state_all_items_without_filling():
    test_state = FSMContext(chat=1, storage=MemoryStorage(), user=1)
    all_items = ('status', 'city', 'category', 'place', 'current_message_id')

    for item_name in all_items:
        item = await get_user_state_item(test_state, item_name)
        if item_name == 'status':
            assert item == 'client'
        elif item_name == 'current_message_id':
            assert item == 0
        else:
            assert isinstance(item, dict) is True
            assert len(item) != 0


@pytest.mark.asyncio
async def test_get_user_state_all_items_without_filling_with_defaults():
    test_state = FSMContext(chat=1, storage=MemoryStorage(), user=1)
    city = {"city": "one"}
    category = {"category": "one"}
    place = {"place": "one"}
    all_items_with_defaults = {'status': "zalupa",
                               'city': city,
                               'category': category,
                               'place': place,
                               'current_message_id': 0
                               }

    for item_name, item_default in all_items_with_defaults.items():
        item = await get_user_state_item(test_state, item_name, default=item_default)
        if item_name == 'status' or item_name == 'current_message_id':
            assert item == item_default
        else:
            assert isinstance(item, dict) is True
            assert item == item_default
            assert len(item) == 1
