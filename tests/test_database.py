from src.tg_places_bot.database.database import connect_to_db
from random import randint, choice


db = connect_to_db()
all_items = ('city', 'tg_user', 'category', 'place')


def get_random_select_args(table_name: str = None, for_keyboard: str = None, params : dict = None):
    values_simple = ('none', 'name', None, 0, True, False)
    values_sequences = ((), [], {}, '')
    table_name = choice(values_simple) if not table_name else table_name
    for_keyboard = choice(values_simple) if not table_name else for_keyboard
    params = choice(values_sequences) if not params else params
    return table_name, for_keyboard, params


# ==================================================================================================================== #
#                                                    SELECT METHOD TESTS                                               #
# ==================================================================================================================== #


def test_select_all_items():
    for table_name in all_items:
        items = db.select(table_name)
        assert isinstance(items, list) is True
        for item in items:
            assert isinstance(item, list) is True


def test_select_one_item():
    for table_name in all_items:
        one_item = db.select(table_name, params={'id': 1})
        assert isinstance(one_item, list) is True
        assert len(one_item) > 1


def test_select_item_one_field():
    table_name = 'city'
    one_item = db.select(table_name, params={'id': 1}, field_index=0)
    assert isinstance(one_item, list) is True
    assert len(one_item) == 1
    assert isinstance(one_item[0], int) is True


def test_select_cities_for_keyboard():
    for table_name in all_items:
        item_keyboard = db.select(table_name, for_keyboard=True)
        assert isinstance(item_keyboard, dict) is True


# ================================================= FIELD INDEXES ==================================================== #


def test_select_item_indexes():
    for table_name in all_items:
        items_indexes = db.select(table_name, field_index=0)
        assert isinstance(items_indexes, list) is True
        for c_i in items_indexes:
            assert isinstance(c_i, int) is True


def test_select_places_photos():
    table_name = 'place'
    places = db.select(table_name, field_index=7)
    for p in places:
        assert isinstance(p, bytes) is True


def test_select_item_doesnt_exist():
    no_table_name = 'none'
    no_item = db.select(no_table_name)
    assert isinstance(no_item, list) is True
    assert len(no_item) == 0


def test_select_item_field_over_index():
    for table_name in all_items:
        field_random_index = randint(20, 200)
        item_columns = db.select(table_name, field_index=field_random_index)
        assert isinstance(item_columns, list) is True
        assert len(item_columns) == 0


def test_select_item_field_negative_index():
    for table_name in all_items:
        field_negative_index = randint(-200, -20)
        items = db.select(table_name, field_index=field_negative_index)
        assert isinstance(items, list) is True
        assert len(items) == 0


def test_select_item_with_empty_params():
    empty_params = {}
    for name in all_items:
        items = db.select(name, params=empty_params)
        assert isinstance(items, list)
        assert len(items) > 1


def test_select_item_random_params():
    test_params = {"not existed param": 1, "code": "msk", "not existed param 2": "Yse", "not existed param 3": None}
    for table_name in all_items:
        items = db.select(table_name, params=test_params)
        assert isinstance(items, list) is True
        assert len(items) == 0


def test_select_item_random_arguments():
    for table_name in all_items:
        table_name, field_index, params = get_random_select_args()
        item = db.select(table_name, field_index=field_index, params=params)
        assert isinstance(item, list) is True
        assert len(item) == 0


def test_select_items_to_dict():
    for table_name in all_items:
        item = db.select(table_name, to_dict=True)
        assert isinstance(item, list) is True
        for row in item:
            assert isinstance(row, dict) is True


def test_select_item_to_dict():
    table_name = 'city'
    items = db.select(table_name, to_dict=True)
    assert isinstance(items, list) is True
    for item in items:
        assert isinstance(item, dict) is True


def test_select_item_by_id_to_dict():
    table_name = 'city'
    item = db.select(table_name, params={"id": 1}, to_dict=True)
    assert isinstance(item, dict) is True


def test_select_item_by_dont_exist_id_to_dict():
    table_name = 'city'
    item = db.select(table_name, params={"id": 1000}, to_dict=True)
    assert isinstance(item, dict) is True
    assert len(item) == 0


# ==================================================================================================================== #
#                                                    INSERT METHOD TESTS                                               #
# ==================================================================================================================== #


def test_insert_item():
    test_data = {
        'city': ('test_1', 'TEST 1', 0.0, 0.0, 'TEST DESCRIPTION', 'smile'),
        'tg_user': (2, 'test_username', 'test_status'),
        'category': ('test_code', 'test_name', 'test_smile'),
        'place': ('Тест', 0.0, 0.0, 1, 'test description 1', b'', 'test smile', 1, 'test user')
    }
    for table_name, table_args in test_data.items():
        item_index = db.insert(table_name, table_args)
        assert isinstance(item_index, int) is True  # check that insert return id (int)
        item = db.select(table_name, params={'id': item_index})
        assert isinstance(item, list) is True
        if table_name == 'tg_user':
            assert item == list(table_args)
        elif table_name == 'place':
            table_args = list((item_index, 'test') + table_args)
            assert item == table_args
        else:
            assert item == list((item_index, ) + table_args)


def test_insert_item_with_result():
    table_name = 'city'
    args = ('test_2', 'TEST 2', 0.1, 0.2, 'TEST DESCRIPTION 2', 'sad')
    full_result = True
    inserted_item = db.insert(table_name, args, full_result)
    assert isinstance(inserted_item, dict) is True
    assert len(inserted_item) > 1
    item = list(inserted_item.values())
    args = list(args)
    args.insert(0, inserted_item['id'])
    assert item == args


def test_insert_items_with_result():
    test_data3 = {
        'city': ('test_3', 'TEST 3', 0.0, 0.0, 'TEST DESCRIPTION 3', 'smile 3'),
        'tg_user': (3, 'test_username 3', 'test_status_3'),
        'category': ('test_code_3', 'test_name 3', 'test_smile 3'),
        'place': ('Тест_2', 0.0, 0.0, 1, 'test description 3', b'', 'test smile 3', 1, 'test user 3')
    }
    for table_name, table_args in test_data3.items():
        item_index = db.insert(table_name, table_args)
        assert isinstance(item_index, int) is True  # check that insert return id (int)
        item = db.select(table_name, params={'id': item_index})
        assert isinstance(item, list) is True
        assert len(item) > 1
        if table_name == 'tg_user':
            assert item == list(table_args)
        elif table_name == 'place':
            table_args = list((item_index, 'test_2') + table_args)
            assert item == table_args
        else:
            assert item == list((item_index, ) + table_args)


def test_insert_with_existed_id():
    table_name = 'city'
    args = (1, 'test_3', 'TEST 3', 1.1, 2.2, 'TEST DESCRIPTION 3', 'happy')
    item1 = db.insert(table_name, args)
    item2 = db.insert(table_name, args, True)
    assert item1 == -1
    assert item2 == -1


# ==================================================================================================================== #
#                                                    UPDATE METHOD TESTS                                               #
# ==================================================================================================================== #

# def test_update_item():
#     test_params = {
#         'city': (1, {'code': 'test_1'}),
#         'tg_user': (1, {'status': 'test_status 2'}),
#         'category': {'code': ('test_code',)},
#         'rating': {'user_id': (940310930,)},
#         'place': {'code': ('test_code',)},
#     }
#     for table_name, params in test_params.items():
#         param_name, param_values = params.popitem()
#         for param_value in param_values:
#             status = db.drop_items(table_name, params={param_name: param_value})
#             assert isinstance(status, int) is True
#             assert status == 1

# ==================================================================================================================== #
#                                                     DROP METHOD TESTS                                                #
# ==================================================================================================================== #


def test_drop_item_by_id():
    test_params = {
        'city': {'code': ('test_1', 'test_2', 'test_3')},
        'tg_user': {'status': ('test_status', 'test_status_3')},
        'category': {'code': ('test_code', 'test_code_3')},
        'rating': {'user_id': (2,)},
        'place': {'code': ('test', 'test_2')},
    }
    for table_name, params in test_params.items():
        param_name, param_values = params.popitem()
        for param_value in param_values:
            status = db.drop_items(table_name, params={param_name: param_value})
            assert isinstance(status, int) is True
            assert status == 1


def test_drop_item_by_not_exist_id():
    item_id = -404
    for table_name in all_items:
        status = db.drop_items(table_name, params={'id': item_id})
        assert isinstance(status, int) is True
        print(status)
        assert status == 1


# ==================================================================================================================== #
#                                                    OTHER METHOD TESTS                                                #
# ==================================================================================================================== #


def test_get_columns():
    for table_name in all_items:
        columns = db.get_table_columns(table_name)
        columns_with_id = db.get_table_columns(table_name, without_id=False)
        assert isinstance(columns, list) is True
        assert isinstance(columns_with_id, list) is True
        assert len(columns) > 1
        assert len(columns) + 1 == len(columns_with_id)


def test_get_columns_with_random_args():
    for table_name in all_items:
        table_name, without_id, without_columns = get_random_select_args()
        columns = db.get_table_columns(table_name, without_id=without_id, without_columns=without_columns)
        assert isinstance(columns, list) is True
        assert len(columns) == 0



